﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TemplateWizard;
using System.Windows.Forms;
using EnvDTE;
using EnvDTE80;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;

namespace HotDecalPluginTemplateInstaller {

    public class DecalHotReloadPluginUBServiceTemplateWizard : IWizard {
        //private DecalHotReloadPluginUBServiceTemplateWizardUI inputForm;
        //private string customMessage;
        private string solutionDir = "";

        private List<string> solutionItems = new List<string>();

        // This method is called before opening any item that
        // has the OpenInEditor attribute.
        public void BeforeOpeningFile(ProjectItem projectItem) {

        }

        public void ProjectFinishedGenerating(Project project) {
            try {
                var projectFolders = Directory.GetDirectories(solutionDir);
                
                foreach (var folder in projectFolders) {
                    if (folder.EndsWith("bin") || folder.EndsWith(".Loader") || folder.EndsWith(".vs")) {
                        continue;
                    }

                    MoveProjectFileToRoot(Path.GetFileName(folder), "deps\\Decal.Adapter.dll", "deps\\Decal.Adapter.dll", false);
                    MoveProjectFileToRoot(Path.GetFileName(folder), "deps\\Decal.Interop.Core.dll", "deps\\Decal.Interop.Core.dll", false);
                    MoveProjectFileToRoot(Path.GetFileName(folder), "README.md", "README.md", true);
                }

                if (solutionItems.Count > 0) {
                    var itemsContent = string.Join("\n", solutionItems.Select(i => $"\t\t{i} = {i}"));
                    var contentToAppend = $"Project(\"{{2150E333-8FDC-42A3-9474-1A3956D46DE8}}\") = \"Solution Items\", \"Solution Items\", \"{{{(new Guid())}}}\"";
                    contentToAppend += "\tProjectSection(SolutionItems) = preProject";
                    contentToAppend += itemsContent;
                    contentToAppend += "\tEndProjectSection";
                    contentToAppend += "EndProject";

                    // sln doesnt exist yet...
                    //var file = Directory.GetFiles(solutionDir).Where(f => f.EndsWith(".sln")).First();
                    //File.AppendAllText(file, contentToAppend);
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
        }

        private void MoveProjectFileToRoot(string projectDir, string originalFile, string newFile, bool addToSolution) {
            try {
                var projectPath = Path.Combine(solutionDir, projectDir);
                var csProjFile = Path.Combine(projectPath, $"{projectDir}.csproj");

                var copyDir = Path.GetDirectoryName(Path.Combine(solutionDir, newFile));
                if (!Directory.Exists(copyDir)) {
                    Directory.CreateDirectory(copyDir);
                }
                
                File.Move(Path.Combine(projectPath, originalFile), Path.Combine(solutionDir, newFile));

                // remove from csproj
                RemoveLinesMatchingRegex(new Regex($"<(Content|None).*Include=\".*{originalFile.Split('\\').Last()}.*\".*>"), csProjFile);

                if (addToSolution) {
                    solutionItems.Add(newFile);
                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }
        }

        private void RemoveLinesMatchingRegex(Regex regex, string filePath) {
            string[] lines = File.ReadAllLines(filePath);
            string[] newLines = Array.FindAll(lines, x => !regex.IsMatch(x));
            File.WriteAllLines(filePath, newLines);
        }

        // This method is only called for item templates,
        // not for project templates.
        public void ProjectItemFinishedGenerating(ProjectItem projectItem) {

        }

        // This method is called after the project is created.
        public void RunFinished() {
            //MessageBox.Show($"RunFinished");
        }

        public void RunStarted(object automationObject, Dictionary<string, string> replacementsDictionary, WizardRunKind runKind, object[] customParams) {
            try {
                solutionDir = replacementsDictionary["$destinationdirectory$"];
            }
            catch (Exception ex) { MessageBox.Show(ex.ToString()); }
            /*
            try {
                // Display a form to the user. The form collects
                // input for the custom message.
                inputForm = new DecalHotReloadPluginUBServiceTemplateWizardUI();
                inputForm.ShowDialog();

                customMessage = DecalHotReloadPluginUBServiceTemplateWizardUI.CustomMessage;

                // Add custom parameters.
                replacementsDictionary.Add("$custommessage$", customMessage);
            }
            catch (Exception ex) {
                MessageBox.Show(ex.ToString());
            }*/
        }

        // This method is only called for item templates,
        // not for project templates.
        public bool ShouldAddProjectItem(string filePath) {
            return true;
        }
    }
}
